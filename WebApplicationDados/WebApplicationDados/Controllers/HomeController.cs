﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplicationDados.Models;
using PagedList;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;

namespace WebApplicationDados.Controllers
{
    public class HomeController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings.Get("urlBase");
        int pageSize = 1000;

        public async Task<ActionResult> Index(int? page)
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }
                int pageNumber = page ?? 1;
                return View(lstDados.ToPagedList(pageNumber,pageSize));
            }
        }

        public async Task<ActionResult> Group(string grupo,int? page)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }
                int pageNumber = page ?? 1;
                return View(lstDados.ToPagedList(pageNumber, pageSize));
            }
        }

        public async Task<ActionResult> ExportToExcel()
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=DadosExcel.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToExcelPaginado(int pagina)
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados.Skip((pagina - 1) * pageSize).Take(pageSize);
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=DadosExcel.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToExcelAgrupado(string grupo)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=DadosExcel.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToExcelAgrupadoPaginado(string grupo, int pagina)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados.Skip((pagina - 1) * pageSize).Take(pageSize);
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=DadosExcel.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                Response.Output.Write(objStringWriter.ToString());
                Response.Flush();
                Response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToCsv()
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }

                var sb = new StringBuilder();
                // You can write sql query according your need  
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7}", "ID", "Data", "Tipo Operação", "Ativo", "Quantidade", "Preço", "Conta", Environment.NewLine);
                foreach (var item in lstDados)
                {
                    sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7}", item.id, item.dateTime.ToShortDateString(), item.tipoOperacao, item.ativo, item.quantidade, item.preco, item.conta, Environment.NewLine);
                }
                //Get Current Response  
                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                response.AddHeader("content-disposition", "attachment;filename=DadosCsv.csv ");
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToCsvPaginado(int pagina)
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }

                var sb = new StringBuilder();
                // You can write sql query according your need  
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7}", "ID", "Data", "Tipo Operação", "Ativo", "Quantidade", "Preço", "Conta", Environment.NewLine);
                foreach (var item in lstDados.Skip((pagina - 1) * pageSize).Take(pageSize))
                {
                    sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7}", item.id, item.dateTime.ToShortDateString(), item.tipoOperacao, item.ativo, item.quantidade, item.preco, item.conta, Environment.NewLine);
                }
                //Get Current Response  
                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                response.AddHeader("content-disposition", "attachment;filename=DadosCsv.csv ");
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToCsvAgrupado(string grupo)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }

                var sb = new StringBuilder();
                // You can write sql query according your need  
                sb.AppendFormat("{0};{1};{2};{3}", (grupo == "C" ? "Conta" : (grupo == "A" ? "Ativo" : "Tipo Operação")), "Preço Médio", "Quantidade", Environment.NewLine);
                foreach (var item in lstDados)
                {
                    sb.AppendFormat("{0};{1};{2};{3}", item.Agrupador, item.PrecoMedio, item.Quantidade, Environment.NewLine);
                }
                //Get Current Response  
                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                response.AddHeader("content-disposition", "attachment;filename=DadosAgrupadosCsv.csv ");
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToCsvAgrupadoPaginado(string grupo, int pagina)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }

                var sb = new StringBuilder();
                // You can write sql query according your need  
                sb.AppendFormat("{0};{1};{2};{3}", (grupo == "C" ? "Conta" : (grupo == "A" ? "Ativo" : "Tipo Operação")), "Preço Médio", "Quantidade", Environment.NewLine);
                foreach (var item in lstDados.Skip((pagina - 1) * pageSize).Take(pageSize))
                {
                    sb.AppendFormat("{0};{1};{2};{3}", item.Agrupador, item.PrecoMedio, item.Quantidade, Environment.NewLine);
                }
                //Get Current Response  
                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                response.AddHeader("content-disposition", "attachment;filename=DadosAgrupadosCsv.csv ");
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();
                return View();
            }
        }

        public async Task<ActionResult> ExportToPdf()
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados;
                gv.DataBind();

                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);

                using (MemoryStream stream = new System.IO.MemoryStream())
                {
                    StringReader sr = new StringReader(objStringWriter.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    pdfDoc.Close();
                    return File(stream.ToArray(), "application/pdf", "DadosPdf.pdf");
                }
            }
        }

        public async Task<ActionResult> ExportToPdfPaginado(int pagina)
        {
            List<Dados> lstDados = new List<Dados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data");
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<Dados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados.Skip((pagina - 1) * pageSize).Take(pageSize);
                gv.DataBind();

                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);

                using (MemoryStream stream = new System.IO.MemoryStream())
                {
                    StringReader sr = new StringReader(objStringWriter.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    pdfDoc.Close();
                    return File(stream.ToArray(), "application/pdf", "DadosPdf.pdf");
                }
            }
        }

        public async Task<ActionResult> ExportToPdfAgrupado(string grupo)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados;
                gv.DataBind();
                
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);
                
                using (MemoryStream stream = new System.IO.MemoryStream())
                {
                    StringReader sr = new StringReader(objStringWriter.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    pdfDoc.Close();
                    return File(stream.ToArray(), "application/pdf", "DadosPdfAgrupados.pdf");
                }
            }
        }

        public async Task<ActionResult> ExportToPdfAgrupadoPaginado(string grupo, int pagina)
        {
            List<DadosAgrupados> lstDados = new List<DadosAgrupados>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/Data/Details/" + grupo);
                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    lstDados = JsonConvert.DeserializeObject<List<DadosAgrupados>>(EmpResponse);
                }

                var gv = new GridView();
                gv.DataSource = lstDados.Skip((pagina - 1) * pageSize).Take(pageSize);
                gv.DataBind();

                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                gv.RenderControl(objHtmlTextWriter);

                using (MemoryStream stream = new System.IO.MemoryStream())
                {
                    StringReader sr = new StringReader(objStringWriter.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    pdfDoc.Close();
                    return File(stream.ToArray(), "application/pdf", "DadosPdfAgrupados.pdf");
                }
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}