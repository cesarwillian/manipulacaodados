# README #

Este repositório possuí 3 aplicações com a finalidade de manipulação de dados usando .NET Framework.

### Qual a finalidade deste repositório? ###

* Composto de 3 aplicações sendo uma para geração de massa, outra para ler e disponibilizar os dados e uma outra para apresentar os dados
* Version: 1.0

### Instruções de execução e configuração ###

#### Primeira parte: ####
* Executar a aplicação GeradorMassaDados - Console Application
* Algumas opções de configuração como diretório e nome do arquivo estão no app.config
* Ao iniciar a aplicação é feita uma verificação e caso já tenha sido gerado um arquivo é solicitada a confirmação para gerar novamente

#### Segunda parte: ####
* Executar a aplicação WebApiDados - WebApi
* Algumas opções de configuração como diretório e nome do arquivo estão no web.config
* Os logs são gerados na raiz da aplicação WebApiDados
* A aplicação WebApiDados deve ficar rodando para que o serviço fique disponível para ser consumido

#### Terceira parte: ####
* Executar a aplicação WebApplicationDados - ASPNET MVC
* Algumas opções de configuração como urlBase da WebApi estão no web.config
* Os dados são retornado de forma paginada devido a grande quantidade de registros
* É possível exportar os dados de forma paginada ou integral nos formatos de Excel, CSV ou PDF
* As opções de agrupamento foram disponibilizadas no formato de combobox na parte superior do site identificado pelo texto "Agrupar por:"
* Ao selecionar uma opção de agrupamento uma nova página será carregada com o agrupamento selecionado
* Todo o tema da WebApplicationDados foi feito para lembrar a aparência do Excel