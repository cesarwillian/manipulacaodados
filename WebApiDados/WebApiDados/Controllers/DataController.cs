﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.OutputCache.V2;
using Newtonsoft.Json;
using WebApiDados.Models;
using log4net;
using System.Diagnostics;
using System.Configuration;

namespace WebApiDados.Controllers
{
    public class DataController : ApiController
    {
        private ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: api/Data
        [HttpGet]
        [CacheOutput(ServerTimeSpan = 120)]
        public IHttpActionResult Index()
        {
            try
            {
                logger.Info("Inicio da leitura dos dados");

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                string diretorio = ConfigurationManager.AppSettings.Get("diretorio");
                string arquivo = ConfigurationManager.AppSettings.Get("arquivo");

                List<Dados> dados = new List<Dados>();

                if (System.IO.File.Exists(diretorio + arquivo))
                {
                    string json = System.IO.File.ReadAllText(diretorio + arquivo);

                    dados = JsonConvert.DeserializeObject<List<Dados>>(json);
                    dados = dados.OrderBy(s => s.id).ToList();
                }

                stopwatch.Stop();

                logger.Info(string.Format("Tempo decorrido: {0} milliseconds", stopwatch.ElapsedMilliseconds));
                logger.Info("Fim da leitura dos dados");

                return Json(dados, new JsonSerializerSettings { Formatting = Formatting.Indented });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Content(HttpStatusCode.BadRequest, new { error = ex.Message });
            }
        }

        // GET: api/Data/Details/param=C,A,T
        [HttpGet]
        [CacheOutput(ServerTimeSpan = 120)]
        public IHttpActionResult Details(string param)
        {
            try
            {
                logger.Info("Inicio da leitura dos dados - agrupado");

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                string diretorio = ConfigurationManager.AppSettings.Get("diretorio");
                string arquivo = ConfigurationManager.AppSettings.Get("arquivo");

                List<Dados> dados = new List<Dados>();

                if (System.IO.File.Exists(diretorio + arquivo))
                {
                    string json = System.IO.File.ReadAllText(diretorio + arquivo);

                    dados = JsonConvert.DeserializeObject<List<Dados>>(json);
                }

                List<DadosAgrupados> lst = new List<DadosAgrupados>();

                switch (param)
                {
                    case "C":
                        lst = dados.GroupBy(u => u.conta).Select(grp => new DadosAgrupados
                        {
                            Agrupador = grp.Key.ToString(),
                            Quantidade = grp.Count(),
                            PrecoMedio = grp.Sum(x => (x.quantidade * x.preco)) / grp.Count()
                        }).ToList();
                        break;
                    case "A":
                        lst = dados.GroupBy(u => u.ativo).Select(grp => new DadosAgrupados
                        {
                            Agrupador = grp.Key.ToString(),
                            Quantidade = grp.Count(),
                            PrecoMedio = grp.Sum(x => (x.quantidade * x.preco)) / grp.Count()
                        }).ToList();
                        break;
                    case "T":
                        lst = dados.GroupBy(u => u.tipoOperacao).Select(grp => new DadosAgrupados
                        {
                            Agrupador = grp.Key.ToString(),
                            Quantidade = grp.Count(),
                            PrecoMedio = grp.Sum(x => (x.quantidade * x.preco)) / grp.Count()
                        }).ToList();
                        break;
                    default:
                        lst = dados.GroupBy(u => u.conta).Select(grp => new DadosAgrupados
                        {
                            Agrupador = grp.Key.ToString(),
                            Quantidade = grp.Count(),
                            PrecoMedio = grp.Sum(x => (x.quantidade * x.preco)) / grp.Count()
                        }).ToList();
                        break;
                }

                lst = lst.OrderBy(s => s.Quantidade).ToList();

                stopwatch.Stop();

                logger.Info(string.Format("Tempo decorrido: {0} milliseconds", stopwatch.ElapsedMilliseconds));
                logger.Info("Fim da leitura dos dados - agrupado");

                return Json(lst, new JsonSerializerSettings { Formatting = Formatting.Indented });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Content(HttpStatusCode.BadRequest, new { error = ex.Message });
            }
        }
    }
}
