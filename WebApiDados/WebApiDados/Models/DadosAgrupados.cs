﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDados.Models
{
    public class DadosAgrupados
    {
        public string Agrupador { get; set; }
        public int Quantidade { get; set; }
        public double PrecoMedio { get; set; }
    }
}