﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeradorMassaDados
{
    public class Dados
    {
        //Identificador numérico da operação
        public long id { get; set; }
        //Campo com a data e hora do evento
        public DateTime dateTime { get; set; }
        //C = compra, V = venda
        public char tipoOperacao { get; set; }
        //Nome da ação na Bolsa, exemplo: PETR4
        public string ativo { get; set; }
        //Quantidade envolvida na operação
        public int quantidade { get; set; }
        //Preco envolvido na operação
        public double preco { get; set; }
        //Conta do cliente envolvida na operação
        public int conta { get; set; }
    }
}
