﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GeradorMassaDados
{
    public class GerarMassa
    {
        Dados[] _lst;
        int _total = Convert.ToInt32(ConfigurationManager.AppSettings.Get("qtd"));
        int _parallel = Convert.ToInt32(ConfigurationManager.AppSettings.Get("parallel"));
        
        public void Gerar(string arquivo)
        {
            Console.WriteLine("Gerando...");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            _lst = new Dados[_total];

            int qtd = _total / _parallel;

            List<Thread> threads = new List<Thread>();

            for (int i = 0; i < _parallel; i++)
            {
                int inicio = (i * qtd) + 1;
                int final = (i + 1) * qtd;
                Thread t = new Thread(() => GerarDados(inicio, final))
                {
                    Name = $"Thread{(i + 1)}"
                };
                t.Priority = ThreadPriority.Normal;
                threads.Add(t);
            }

            foreach (var item in threads)
            {
                item.Start();
            }

            bool running = true;

            while (running)
            {
                running = false;
                foreach (var item in threads)
                {
                    running = item.IsAlive;
                }
            }

            stopwatch.Stop();
            Console.WriteLine($"Tempo decorrido: {stopwatch.ElapsedMilliseconds}");

            string json = JsonConvert.SerializeObject(_lst);
            string diretorio = ConfigurationManager.AppSettings.Get("diretorio");
            if (!Directory.Exists(diretorio))
                Directory.CreateDirectory(diretorio);
            File.WriteAllText(diretorio + arquivo, json);
        }

        public void GerarDados(int inicio, int final)
        {
            try
            {
                List<Dados> lstDados = new List<Dados>();
                
                Random rnd = new Random();

                Console.WriteLine($"{Thread.CurrentThread.Name} | valor inicial: {inicio} | valor final: {final}");

                for (int i = inicio; i <= final; i++)
                {
                    Dados dados = new Dados();
                    dados.id = i;
                    dados.dateTime = DateTime.Now;
                    dados.tipoOperacao = rnd.Next(1000) > 500 ? 'C' : 'V';
                    dados.ativo = GerarAtivo();
                    dados.quantidade = Convert.ToInt32(GerarValorAleatorio(3, "N"));
                    dados.preco = GerarPreco();
                    dados.conta = Convert.ToInt32(GerarValorAleatorio(4, "N"));

                    _lst[(i - 1)] = dados;
                    Thread.Sleep(10);
                }
            }
            catch (ThreadAbortException ex)
            {
                Console.WriteLine($"Thread(ERROR): {Thread.CurrentThread.Name}");
                Console.WriteLine(ex.Message);
            }
        }

        public string GerarAtivo()
        {
            return GerarValorAleatorio(4, "C") + GerarValorAleatorio(1, "N");
        }

        public double GerarPreco()
        {
            return Convert.ToDouble(GerarValorAleatorio(3, "N") + "," + GerarValorAleatorio(2, "N"));
        }

        public string GerarValorAleatorio(int tamanho, string tipo)
        {
            string result = string.Empty;
            var chars = tipo.Equals("C") ? "ABCDEFGHIJKLMNOPQRSTUVWXYZ" : "123456789";
            var random = new Random();
            result = new string(
                Enumerable.Repeat(chars, tamanho)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }
}
