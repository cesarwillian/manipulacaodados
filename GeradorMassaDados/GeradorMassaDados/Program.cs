﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeradorMassaDados
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inicio da geração da massa de dados");
            try
            {
                string diretorio = ConfigurationManager.AppSettings.Get("diretorio");
                string arquivo = ConfigurationManager.AppSettings.Get("arquivo");
                if (File.Exists(diretorio + arquivo))
                {
                    Console.WriteLine("A massa já foi gerada. Deseja gerar novamente? s/n");
                    string valor = Console.ReadLine();
                    if (valor.ToLower().Equals("s"))
                    {
                        File.Delete(arquivo);
                        GerarMassa massa = new GerarMassa();
                        massa.Gerar(arquivo);
                    }
                }
                else
                {
                    GerarMassa massa = new GerarMassa();
                    massa.Gerar(arquivo);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Fim da geração da massa de dados");
            Console.WriteLine(" ");
            Console.WriteLine("Pressione qualquer tecla para finalizar...");
            Console.ReadKey();
        }
    }
}
